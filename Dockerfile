FROM golang:1.11 as gobuilder

ENV GO_VERSION=1.11 \
  GOPATH=/go \
  GODEP_VERSION=0.5.0

WORKDIR /go/src/gitlab.com/virago/irc

ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

# install godep
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin"
RUN wget --no-check-certificate https://github.com/golang/dep/releases/download/v${GODEP_VERSION}/dep-linux-amd64 -O "$GOPATH/bin/dep"
RUN chmod -R 777 "$GOPATH"

COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure -vendor-only -v

COPY . .

RUN go build -o main main.go
CMD ["./main"]
EXPOSE 6667

#stretch
FROM debian:stretch
WORKDIR /irc
RUN mkdir -p log
COPY --from=gobuilder /go/src/gitlab.com/virago/irc/main .

CMD ["./main"]
EXPOSE 6667