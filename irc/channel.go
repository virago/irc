package irc

import (
	log "github.com/sirupsen/logrus"
	"github.com/sorcix/irc"
	"sort"
	"strings"
	"sync"
	"time"
)

type Channel struct {
	sync.RWMutex
	createTime time.Time
	name       string
	server     *Server
	clients    *ConnectedClients
}

type ConnectedChannels struct {
	sync.RWMutex
	channel map[string]*Channel
}

func NewConnectedChannels() *ConnectedChannels {
	return &ConnectedChannels{
		channel: make(map[string]*Channel),
	}
}

func (cc *ConnectedChannels) Add(c *Channel) error {
	cc.Lock()
	defer cc.Unlock()
	if _, exits := cc.channel[c.name]; !exits {
		cc.channel[c.name] = c
	}

	return nil
}

func (cc *ConnectedChannels) GetChannelByName(name string) *Channel {
	cc.Lock()
	defer cc.Unlock()
	if channel, ok := cc.channel[name]; ok {
		return channel
	}

	return nil
}

func NewChannel(server *Server, name string) *Channel {
	return &Channel{
		createTime: time.Now(),
		server:     server,
		name:       name,
		clients:    NewConnectedClient(),
	}
}

func (ch *Channel) Join(c *Client) error {
	var messages []*irc.Message
	ch.Lock()
	defer ch.Unlock()
	if err := ch.clients.Add(c); err != nil {
		return err
	}

	msg := &irc.Message{
		Prefix:  c.Prefix(),
		Command: irc.JOIN,
		Params:  []string{ch.name},
	}
	for _, to := range ch.clients.GetAllClients() {
		err := to.Encode(msg)
		if err != nil {
			log.Errorf("failed encode msg %s", err)
		}
	}

	messages = append(messages,
		&irc.Message{
			Prefix:   ch.server.Prefix(),
			Command:  irc.RPL_NAMREPLY,
			Params:   []string{c.nick, "=", ch.name},
			Trailing: strings.Join(ch.Names(), " "),
		},
		&irc.Message{
			Prefix:   ch.server.Prefix(),
			Params:   []string{c.nick, ch.name},
			Command:  irc.RPL_ENDOFNAMES,
			Trailing: "End of /NAMES list.",
		},
	)

	return c.Encode(messages...)
}

func (ch *Channel) Part(c *Client, text string) {
	ch.Lock()
	defer ch.Unlock()
	msg := &irc.Message{
		Prefix:   c.Prefix(),
		Command:  irc.PART,
		Params:   []string{ch.name},
		Trailing: text,
	}

	if client := ch.clients.GetClientByName(c.nick); client == nil {
		err := c.Encode(&irc.Message{
			Prefix:   ch.server.Prefix(),
			Command:  irc.ERR_NOTONCHANNEL,
			Params:   []string{ch.name},
			Trailing: "You're not on that channel",
		})

		log.Errorf("Cannot send error message to user. UserName: %s, Error: %s", c.nick, err.Error())
		return
	}

	for _, client := range ch.clients.GetAllClients() {
		if err := client.Encode(msg); err != nil {
			log.Errorf("Cannot send part message to user. UserName: %s, Error: %s", c.nick, err.Error())
		}
	}

	if err := ch.clients.Remove(c); err != nil {
		log.Errorf("Cannot remove user from channel. UserName: %s, Error: %s", c.nick, err.Error())
	}
}

func (ch *Channel) Names() []string {
	names := make([]string, 0, ch.clients.Count())
	for _, u := range ch.clients.GetAllClients() {
		names = append(names, u.nick)
	}

	sort.Strings(names)
	return names
}

func (ch *Channel) SendMessage(from *Client, text string) error {
	ch.RLock()
	defer ch.RUnlock()

	msg := &irc.Message{
		Prefix:   from.Prefix(),
		Command:  irc.PRIVMSG,
		Params:   []string{ch.name},
		Trailing: text,
	}

	for _, client := range ch.clients.GetAllClients() {
		if client.nick == from.nick {
			continue
		}
		err := client.Encode(msg)
		if err != nil {
			log.Errorf("Cannot send message to user. %s", client.nick)
		}
	}

	return nil
}
