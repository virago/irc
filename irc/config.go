package irc

import (
	"github.com/jinzhu/configor"
	log "github.com/sirupsen/logrus"
)

var Config DefaultConfig

type DefaultConfig struct {
	Env    string `default:"dev" env:"ENV"`
	Socket struct {
		Port string `default:":6667" env:"PORT"`
	}
	Server struct {
		Name string `default:"localhost.server" env:"SERVER_NAME"`
	}
}

func init() {
	err := configor.New(&configor.Config{ErrorOnUnmatchedKeys: true}).Load(&Config)

	if Config.Env == "dev" {
		log.SetLevel(log.DebugLevel)
	}
	if err != nil {
		panic(err)
	}
}
