package irc

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/sorcix/irc"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type Server struct {
	config   *DefaultConfig
	clients  *ConnectedClients
	channels *ConnectedChannels
	sync.RWMutex
	created      time.Time
	GracefulStop chan os.Signal
}

func NewServer(config *DefaultConfig) *Server {
	server := &Server{
		config:       config,
		GracefulStop: make(chan os.Signal, 2),
		created:      time.Now(),
		clients:      NewConnectedClient(),
		channels:     NewConnectedChannels(),
	}

	signal.Notify(server.GracefulStop, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT)

	return server
}

func (s *Server) Prefix() *irc.Prefix {
	return &irc.Prefix{Name: s.config.Server.Name}
}

func (s *Server) Name() string {
	return s.config.Server.Name
}

func (s *Server) Connect(c *Client) error {
	err := s.registerClient(c)
	if err != nil {
		return err
	}
	go s.handle(c)

	return nil
}

func (s *Server) Run(socket net.Listener) {
	for {
		conn, err := socket.Accept()
		if err != nil {
			log.Errorf("Failed to accept connection: %v", err)
			return
		}

		go func() {
			log.Infof("New connection: %s", conn.RemoteAddr())
			err = s.Connect(NewClient(conn))
			if err != nil {
				log.Errorf("Failed to join: %v", err)
				return
			}
		}()
	}
}

func (s *Server) Shutdown() {
	if command, ok := Commands["NOTICE"]; ok {
		for _, client := range s.clients.GetAllClients() {
			msg := &irc.Message{
				Prefix:   s.Prefix(),
				Command:  irc.NOTICE,
				Params:   []string{client.nick},
				Trailing: "Server is shutting down",
			}
			err := command.handler(s, client, msg)
			if err != nil {
				log.Errorf("handler error for %s", err.Error())
			}
		}

	}

	log.Debug("Server is shutting down")
}

func (s *Server) registerClient(c *Client) error {
	c.host = c.connection.ResolveHost()

	for i := 20; i > 0; i-- {
		msg, err := c.Decode()
		if err != nil {
			return err
		}
		if msg == nil {
			continue
		}

		switch msg.Command {
		case irc.NICK:
			c.nick = msg.Params[0]
		case irc.USER:
			c.user = msg.Params[0]
			c.realName = msg.Trailing
		case irc.PASS:
			if msg.Params[0] == "1234" {
				c.authorized = true
			}
		}

		if c.nick == "" || c.user == "" {
			continue
		}

		return s.addClientToServer(c)
	}

	return nil
}

func (s *Server) addClientToServer(c *Client) error {
	if !c.authorized {
		err := c.Encode(
			&irc.Message{
				Prefix:   nil,
				Command:  irc.ERROR,
				Trailing: "Password incorrect",
			},
			&irc.Message{
				Prefix:   c.Prefix(),
				Command:  irc.QUIT,
				Trailing: "Connection closed",
			})
		if err != nil {
			return err
		}

		return errors.New("Password Incorrect")
	}

	err := s.clients.Add(c)
	if err != nil {
		return err
	}

	return nil
}

func (s *Server) quit(c *Client, message string) {
	go c.Close(message)

	if err := s.clients.Remove(c); err != nil {
		log.Error(err)
	}
}

func (s *Server) handle(c *Client) {
	defer s.quit(c, "Closed.")

	for {
		msg, err := c.Decode()
		if err != nil {
			log.Errorf("handle decode error for %s", err.Error())
			return
		}
		if msg == nil {
			// Ignore empty messages
			continue
		}
		if command, ok := Commands[msg.Command]; ok {
			err = command.handler(s, c, msg)
			if err != nil {
				log.Errorf("handler error for %s", err.Error())
				return
			}
		} else {
			log.Errorf("Command doesn't implemented. %s", msg.Command)
		}
	}
}

func (s *Server) GetChannelByName(name string) *Channel {
	return s.channels.GetChannelByName(name)
}
