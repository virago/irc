package irc

import (
	log "github.com/sirupsen/logrus"
	"github.com/sorcix/irc"
	"strings"
)

const (
	PASS    = "PASS"
	NICK    = "NICK"
	USER    = "USER"
	QUIT    = "QUIT"
	JOIN    = "JOIN"
	PART    = "PART"
	NAMES   = "NAMES"
	PRIVMSG = "PRIVMSG"
	NOTICE  = "NOTICE"
	PING    = "PING"
	PONG    = "PONG"
	ERROR   = "ERROR"
)

type Command struct {
	handler   func(s *Server, c *Client, msg *irc.Message) error
	minParams int
}

var Commands map[string]Command

func init() {
	Commands = map[string]Command{
		JOIN: {
			handler:   joinHandler,
			minParams: 1,
		},
		NAMES: {
			handler:   namesHandler,
			minParams: 0,
		},
		NOTICE: {
			handler:   noticeHandler,
			minParams: 2,
		},
		PART: {
			handler:   partHandler,
			minParams: 1,
		},
		PASS: {
			handler:   passHandler,
			minParams: 1,
		},
		PING: {
			handler:   pingHandler,
			minParams: 1,
		},
		PRIVMSG: {
			handler:   privmsgHandler,
			minParams: 2,
		},
		QUIT: {
			handler:   quitHandler,
			minParams: 0,
		},
	}
}

// joinHandler implements /JOIN command
func joinHandler(s *Server, c *Client, msg *irc.Message) error {
	channels := strings.Split(msg.Params[0], ",")
	for _, channel := range channels {
		ch := s.channels.GetChannelByName(channel)
		if ch == nil {
			ch = NewChannel(s, channel)
		}
		err := ch.Join(c)
		if err != nil {
			log.Error(err)
		}
		err = s.channels.Add(ch)
		if err != nil {
			log.Error(err)
		}
	}

	return nil
}

// namesHandler implements /NAMES command
func namesHandler(s *Server, c *Client, msg *irc.Message) error {
	var r []*irc.Message
	channels := msg.Params
	for _, channel := range channels {
		ch := s.GetChannelByName(channel)
		if ch == nil {
			continue
		}
		msg := irc.Message{
			Prefix:   s.Prefix(),
			Command:  irc.RPL_NAMREPLY,
			Params:   []string{c.nick, "=", channel},
			Trailing: strings.Join(ch.Names(), " "),
		}
		r = append(r, &msg)
	}
	endParams := []string{c.nick}
	if len(channels) == 1 {
		endParams = append(endParams, channels[0])
	}
	r = append(r, &irc.Message{
		Prefix:   s.Prefix(),
		Params:   endParams,
		Command:  irc.RPL_ENDOFNAMES,
		Trailing: "End of /NAMES list.",
	})
	return c.Encode(r...)
}

// noticeHandler implements /NOTICE command
func noticeHandler(s *Server, c *Client, msg *irc.Message) error {
	return c.Encode(msg)
}

// partHandler implements /PART command
func partHandler(s *Server, c *Client, msg *irc.Message) error {
	channels := strings.Split(msg.Params[0], ",")
	for _, chName := range channels {
		ch := s.GetChannelByName(chName)
		if ch == nil {
			err := c.Encode(&irc.Message{
				Prefix:   s.Prefix(),
				Command:  irc.ERR_NOSUCHCHANNEL,
				Params:   []string{chName},
				Trailing: "No such channel",
			})
			log.Error(err)
			continue
		}

		ch.Part(c, msg.Trailing)
	}

	return nil
}

// passHandler implements /PASS command
func passHandler(s *Server, c *Client, msg *irc.Message) error {
	password := msg.Params[0]

	println(password)
	return nil
}

// pingHandler implements /PING command
func pingHandler(s *Server, c *Client, msg *irc.Message) error {
	return c.Encode(&irc.Message{
		Prefix:   s.Prefix(),
		Command:  irc.PONG,
		Params:   []string{s.Name()},
		Trailing: msg.Trailing,
	})
}

//privmsgHandler implements /PRIVMSG command
func privmsgHandler(s *Server, c *Client, msg *irc.Message) error {
	query := msg.Params[0]
	if toChan := s.GetChannelByName(query); toChan != nil {
		toChan.SendMessage(c, msg.Trailing)
	} else if toUser := s.clients.GetClientByName(query); toUser != nil {
		return toUser.Encode(&irc.Message{
			Prefix:   c.Prefix(),
			Command:  irc.PRIVMSG,
			Params:   []string{toUser.nick},
			Trailing: msg.Trailing,
		})
	} else {
		return c.Encode(&irc.Message{
			Prefix:   s.Prefix(),
			Command:  irc.ERR_NOSUCHNICK,
			Params:   msg.Params,
			Trailing: "No such nick/channel",
		})
	}

	return nil
}

//quitHandler implements /QUIT command
func quitHandler(s *Server, c *Client, msg *irc.Message) error {
	return c.Encode(&irc.Message{
		Prefix:   c.Prefix(),
		Command:  irc.QUIT,
		Trailing: msg.Trailing,
	})
}
