// +build integration

package irc_test

import (
	"bufio"
	ircMessage "github.com/sorcix/irc"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"gitlab.com/virago/irc/irc"
	"net"
	"net/textproto"
	"testing"
	"github.com/ory/dockertest"
	"fmt"
)


type IrcTest struct {
	suite.Suite
	pool *dockertest.Pool
	resource *dockertest.Resource
}

func (i *IrcTest) SetupSuite() {
	var err, poolError error
	i.pool, poolError = dockertest.NewPool("")
	if poolError != nil {
		log.Fatalf("Could not connect to docker: %s", poolError)
	}

	i.resource, err = i.pool.BuildAndRun("irc-test", "./../Dockerfile", []string{})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	if err = i.pool.Retry(func() error {
		_ , err := net.Dial("tcp", fmt.Sprintf("127.0.0.1:%s", i.resource.GetPort("6667/tcp")))
		return err
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

}

func (i *IrcTest) TearDownSuite() {
	i.pool.Purge(i.resource)
}

func (i *IrcTest) createConnection() (net.Conn, error) {
	return net.Dial("tcp", fmt.Sprintf("127.0.0.1:%s", i.resource.GetPort("6667/tcp")))
}

func (i *IrcTest) TestInvalidPassWord() {
	conn, _ := i.createConnection()
	client := irc.NewClient(conn)
	err := client.Encode(
		//user1
		&ircMessage.Message{
			Command: "CAP",
			Params:  []string{"LS", "302"},
		},
		&ircMessage.Message{
			Command: irc.PASS,
			Params:  []string{"12345"},
		},
		&ircMessage.Message{
			Command: irc.NICK,
			Params:  []string{"user1"},
		},
		&ircMessage.Message{
			Command:  irc.USER,
			Params:   []string{"user1", "user1", "127.0.0.1"},
			Trailing: "realname",
		},
	)

	reader := bufio.NewReader(conn)
	tp := textproto.NewReader(reader)
	if err != nil {
		i.T().Error("encode error")
	}
	for {
		line, _ := tp.ReadLine()
		m := ircMessage.ParseMessage(string(line))
		if m.Command == irc.ERROR {
			i.Equal("Password incorrect", m.Trailing)
			break
		}
	}
}

func (i *IrcTest) TestJoinChannel() {
	conn, _ := i.createConnection()
	client := irc.NewClient(conn)
	err := client.Encode(
		//user1
		&ircMessage.Message{
			Command: "CAP",
			Params:  []string{"LS", "302"},
		},
		&ircMessage.Message{
			Command: irc.PASS,
			Params:  []string{"1234"},
		},
		&ircMessage.Message{
			Command: irc.NICK,
			Params:  []string{"user1"},
		},
		&ircMessage.Message{
			Command:  irc.USER,
			Params:   []string{"user1", "user1", "127.0.0.1"},
			Trailing: "realname",
		},
		&ircMessage.Message{
			Command: irc.JOIN,
			Params:  []string{"#test"},
		},
		&ircMessage.Message{
			Command: irc.NAMES,
			Params:  []string{"#test"},
		},
	)

	reader := bufio.NewReader(conn)
	tp := textproto.NewReader(reader)
	if err != nil {
		i.T().Error("encode error")
	}
	for {
		line, _ := tp.ReadLine()
		m := ircMessage.ParseMessage(string(line))
		if m.Command == "353" {
			i.Equal("user1", m.Params[0])
			break
		}
	}
}

func (i *IrcTest) TestPrivateMessage() {
	conn, _ := i.createConnection()
	client := irc.NewClient(conn)
	err := client.Encode(
		&ircMessage.Message{
			Command: "CAP",
			Params:  []string{"LS", "302"},
		},
		&ircMessage.Message{
			Command: irc.PASS,
			Params:  []string{"1234"},
		},
		&ircMessage.Message{
			Command: irc.NICK,
			Params:  []string{"user3"},
		},
		&ircMessage.Message{
			Command:  irc.USER,
			Params:   []string{"user3", "user3", "127.0.0.1"},
			Trailing: "realname",
		},
		&ircMessage.Message{
			Command: irc.JOIN,
			Params:  []string{"#test"},
		},
	)
	if err != nil {
		i.T().Error("encode error")
	}
	conn2, _ := i.createConnection()
	client2 := irc.NewClient(conn2)
	err = client2.Encode(
		&ircMessage.Message{
			Command: "CAP",
			Params:  []string{"LS", "302"},
		},
		&ircMessage.Message{
			Command: irc.PASS,
			Params:  []string{"1234"},
		},
		&ircMessage.Message{
			Command: irc.NICK,
			Params:  []string{"user4"},
		},
		&ircMessage.Message{
			Command:  irc.USER,
			Params:   []string{"user4", "user4", "127.0.0.1"},
			Trailing: "realname",
		},
		&ircMessage.Message{
			Command: irc.JOIN,
			Params:  []string{"#test"},
		},
		&ircMessage.Message{
			Command:  irc.PRIVMSG,
			Params:   []string{"user3"},
			Trailing: "How are you?",
		},
	)
	if err != nil {
		i.T().Error("encode error")
	}
	reader := bufio.NewReader(conn)
	tp := textproto.NewReader(reader)
	for {
		line, _ := tp.ReadLine()
		m := ircMessage.ParseMessage(string(line))
		if m.Command == irc.PRIVMSG {
			i.Equal("How are you?", m.Trailing)
			i.Equal(m.Prefix.Name, "user4")

			break
		}
	}
}

func TestSuite(t *testing.T) {
	testSuite := new(IrcTest)
	suite.Run(t, testSuite)
}
