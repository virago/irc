package irc

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/sorcix/irc"
	"net"
	"sync"
)

type Client struct {
	sync.RWMutex
	connection Conn
	nick       string
	realName   string
	user       string
	host       string
	authorized bool
	channels   map[*Channel]bool
}

type ConnectedClients struct {
	nick map[string]*Client
	sync.RWMutex
}

func NewConnectedClient() *ConnectedClients {
	return &ConnectedClients{
		nick: make(map[string]*Client),
	}
}

func (cc *ConnectedClients) Add(c *Client) error {
	cc.Lock()
	defer cc.Unlock()
	if _, exits := cc.nick[c.nick]; exits {
		return fmt.Errorf("duplicate client %s", c.nick)
	}

	cc.nick[c.nick] = c

	return nil
}

func (cc *ConnectedClients) Remove(c *Client) error {
	cc.Lock()
	defer cc.Unlock()
	delete(cc.nick, c.nick)

	return nil
}

func (cc *ConnectedClients) Count() int {
	cc.RLock()
	defer cc.RUnlock()
	return len(cc.nick)
}

func (cc *ConnectedClients) GetAllClients() (result []*Client) {
	cc.RLock()
	defer cc.RUnlock()
	result = make([]*Client, len(cc.nick))
	i := 0
	for _, client := range cc.nick {
		result[i] = client
		i++
	}
	return
}

func (cc *ConnectedClients) GetClientByName(name string) *Client {
	cc.RLock()
	defer cc.RUnlock()
	if client, exits := cc.nick[name]; exits {
		return client
	}

	return nil
}

func NewClient(connection net.Conn) *Client {
	con := &conn{
		Conn:    connection,
		Encoder: irc.NewEncoder(connection),
		Decoder: irc.NewDecoder(connection),
	}

	return &Client{
		connection: con,
		host:       "*",
		authorized: false,
	}
}

func (c *Client) Close(message string) error {
	for ch := range c.channels {
		ch.Part(c, message)
	}
	return c.connection.Close()
}

func (c *Client) Prefix() *irc.Prefix {
	return &irc.Prefix{
		Name: c.nick,
		User: c.user,
		Host: c.host,
	}
}

func (c *Client) Encode(messages ...*irc.Message) (err error) {
	for _, msg := range messages {
		log.Debugf("-> %s", msg)
		err := c.connection.Encode(msg)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Client) Decode() (*irc.Message, error) {
	msg, err := c.connection.Decode()

	if err == nil && msg != nil {
		log.Debugf("<- %s", msg)
	}
	return msg, err
}
