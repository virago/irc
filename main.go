package main

import (
	"gitlab.com/virago/irc/irc"
	"net"
)

func main() {
	socket, err := net.Listen("tcp", irc.Config.Socket.Port)
	if err != nil {
		panic(err)
	}
	defer socket.Close()
	server := irc.NewServer(&irc.Config)
	go server.Run(socket)

	select {
	case <-server.GracefulStop:
		server.Shutdown()
		return
	}
}
